import 'dart:io';

import 'package:flutter/material.dart';
import 'package:innoapsion/constants/colors.dart';
import 'package:innoapsion/services/bitcoin_svc.dart';
import 'package:innoapsion/services/genericwebservice.dart';
import 'package:innoapsion/views/home.dart';
import 'package:toast/toast.dart';

class Splash extends StatefulWidget {
  const Splash({Key key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  String _valorBitcoin = "0";
  String _fechaBitcoin = "";

  @override
  void initState() {
    obtenerValorBitcoin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: SColors.Splash,
      body: FutureBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                /*Center(
                    child: Text(
                  "Water Coin",
                  style: TextStyle(
                      color: Colors.black, fontSize: 100, fontFamily: "Anamortee"),
                )),*/
                Center(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Image(
                      image: AssetImage('assets/images/inno.png'),
                    ),
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                CircularProgressIndicator()
              ],
            );
          } else {
            return Home(valorBitcoin: _valorBitcoin, fechaBitcoin: _fechaBitcoin);
          }
        },
        future: Future.delayed(Duration(seconds: 5)),
      ),
    );
  }

  obtenerValorBitcoin() async {
    try{
      var salida = await GenericWebservice().load(BitcoinService.all);
      setState(() {
        _valorBitcoin = salida['peso'];
        _fechaBitcoin = salida['fecha'];
      });
    } on Exception catch (_) {
      Toast.show("No tiene coneccion a Internet", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
    }
  }

}
