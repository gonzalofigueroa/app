import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:innoapsion/constants/colors.dart';
import 'package:innoapsion/constants/styles.dart';
import 'package:innoapsion/services/bitcoin_svc.dart';
import 'package:innoapsion/services/genericwebservice.dart';
import 'package:toast/toast.dart';

class Home extends StatefulWidget {
  Home({Key key, this.valorBitcoin, this.fechaBitcoin}): super(key: key );
  String valorBitcoin;
  String fechaBitcoin;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  String _valorBitcoin;
  String _fechaBitcoin;
  
    @override
    void initState() {
      _valorBitcoin = widget.valorBitcoin;
      _fechaBitcoin = widget.fechaBitcoin;
      super.initState();
    } 
     @override
    void dispose() { 
      super.dispose();
    }

    @override
    Widget build(BuildContext context) {
      //double _btnPosition = (MediaQuery.of(context).size.width * 0.2).toDouble();
      
      return Scaffold(
        appBar: AppBar(
          title: Text(
            "Valor Bitcoin",
            style: SStyles.AppBar,
            textAlign: TextAlign.center,
          ),
          centerTitle: true,
        ),
        body: Container(
          //alignment: AlignmentGeometry,
          height: MediaQuery.of(context).size.height,
          child: Column( 
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 45, 0, 40),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    //Icon(FontAwesomeIcons.exclamationTriangle, size: 200, color: WCColors.opaqueBlue,)
                    Icon(FontAwesomeIcons.bitcoin, size: 200, color: SColors.IconColor,)
                  ],
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Valor de Bictoin de Hoy", style: SStyles.TituloBitcoin,),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(_valorBitcoin, style: SStyles.ValorBitcoin,),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("fecha de valor: "+_fechaBitcoin, style: SStyles.FechaBitcoin,),
                  ],
                ),
              ),
               Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 30),
                    child: RaisedButton(
                      onPressed: () {
                        recargarValorBitcoin();
                      },
                      child: const Text("Recargar Valor"),
                      color: SColors.BtnColor,
                      textColor: Colors.white,
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
              ),
               )
              // Row(
              //   crossAxisAlignment: CrossAxisAlignment.end,
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: <Widget>[
              //     Padding(
              //       padding: EdgeInsets.only(bottom: 10),
              //       child: RaisedButton(
              //           padding: const EdgeInsets.all(15),
              //           textColor: Colors.white,
              //           color: SColors.BtnColor,
              //           onPressed: (){
              //             recargarValorBitcoin();
              //           },
              //           child: Text("Recargar Valor"),
              //           shape: RoundedRectangleBorder(
              //             borderRadius: BorderRadius.circular(10),
              //           ),
              //         ),
              //     ),
              //   ],
              // )
            ]
          ),
        )
      );
    }
    

  recargarValorBitcoin() async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Center(child: CircularProgressIndicator(),);
    });
    try{
    var salida = await GenericWebservice().load(BitcoinService.all);
    setState(() {
      _valorBitcoin = salida['peso'];
      _fechaBitcoin = salida['fecha'];
    });
    } on Exception catch (_) {
      Toast.show("No tiene coneccion a Internet", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
    }
    Navigator.pop(context);
  }

}
