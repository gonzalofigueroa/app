import 'package:flutter/material.dart';
import 'package:innoapsion/views/splash.dart';

import 'constants/colors.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: SColors.Sbase.shade900,
        dividerColor: Colors.transparent,
        appBarTheme: AppBarTheme(
          color: SColors.AppBar,
          brightness: Brightness.light,
          elevation: 0,
        )
      ),
      home: Splash()
    );
  }

}