import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

abstract class Environment {

  static String urlRelease = 'http://142.44.247.62:85/api';
  static String urlDebug = 'http://142.44.247.62:85/api';

  static DateFormat localShortdate = new DateFormat('dd-MM-yyyy');
  static DateFormat localShorttime = new DateFormat('HH:mm');


  static String wsSiteUrl() {
    if (kReleaseMode) {
      return urlRelease;
    }
    else {
      return urlDebug;
    }
  }

}