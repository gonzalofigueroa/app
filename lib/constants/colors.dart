import 'package:flutter/material.dart';

abstract class SColors {

  static const Sbase = MaterialColor(0xFF162330, {
    50:Color.fromRGBO (22, 35, 48, .05),
    100:Color.fromRGBO(22, 35, 48, .2),
    200:Color.fromRGBO(22, 35, 48, .3),
    300:Color.fromRGBO(22, 35, 48, .4),
    400:Color.fromRGBO(22, 35, 48, .5),
    500:Color.fromRGBO(22, 35, 48, .6),
    600:Color.fromRGBO(22, 35, 48, .7),
    700:Color.fromRGBO(22, 35, 48, .8),
    800:Color.fromRGBO(22, 35, 48, .9),
    900:Color.fromRGBO(22, 35, 48, 1),

  });

  static const Splash = Color(0xFF162330);
  static const AppBar = Color(0xFF162330);
  static const IconColor = Color(0xFF162330);
  static const BtnColor = Color(0xFFe2120f);

}