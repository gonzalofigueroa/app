import "package:flutter/material.dart";

import 'colors.dart';


class SStyles {

  static const TextStyle AppBar = TextStyle(
    color: Colors.white,
    fontSize: 18,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.0,
    fontFamily: 'Lato',
    decoration: TextDecoration.none,
  );

  static const TextStyle TituloBitcoin = TextStyle(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.0,
    fontFamily: 'Lato',
    decoration: TextDecoration.none,
  );


  static const TextStyle ValorBitcoin = TextStyle(
    color: Colors.black,
    fontSize: 25,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.0,
    fontFamily: 'Lato',
    decoration: TextDecoration.none,
  );

  static const TextStyle FechaBitcoin = TextStyle(
    color: Colors.black,
    fontSize: 13,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.0,
    fontFamily: 'Lato',
    decoration: TextDecoration.none,
  );

}