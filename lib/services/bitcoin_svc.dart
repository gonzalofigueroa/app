import 'dart:convert';
import 'package:innoapsion/constants/environment.dart';
import 'package:innoapsion/services/genericwebservice.dart';

class BitcoinService {
  
  static Resource<Map<String, dynamic>> get all {

    return Resource(
      url: Environment.wsSiteUrl(),
      //controller: 'sync',
      action: 'bitcoin',
      parse: (response) {
        final result = json.decode(response.body);
        if (result['peso'] != "") {
          var salida = {
            "peso" : result['peso'],
            "fecha": result['fecha']
          };
          return salida;
        }
        else {
          var salida = {
            "peso" : "",
            "fecha": ""
          };
          return salida;
        }
        
      }
    );

  }
}