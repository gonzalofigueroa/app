import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Resource<T> {
  final String url;
  final String controller;
  final String action;
  T Function(Response response) parse;

  Resource({this.url, this.controller, this.action, this.parse});
}

class GenericWebservice {

  Future<T> load<T>(Resource<T> resource) async {

    //final String service = resource.url + '/' + resource.controller + '/' + resource.action;
    final String service = resource.url + '/' + resource.action;
    try{ 
      final response = await http.get(
        service
      );

      if(response.statusCode == 200) {
        return resource.parse(response);
      } else {
        throw Exception('Failed to load data!');
      }
    } on SocketException catch (_) {
      throw Exception('Failed to load data!');
    }
  }

  Future<T> send<T>(Resource<T> resource, String jsonData) async {

    //final String service = resource.url + '/' + resource.controller + '/' + resource.action;
    final String service = resource.url + '/' + resource.action;

    final Map headers = <String, String>{
      'Content-type' : 'application/json; charset=utf-8', 
      'Accept': 'application/json',
    };

    final response = await http.post(
      service, 
      body: jsonData, 
      headers: headers
    );

    if(response.statusCode == 200) {
      return resource.parse(response);
    } else {
      //print(response.body);
      throw Exception('Failed to post data!');
    }
  }

}